<?php
/**
 * @file
 * This blue-style-template includes the popeye field_formatter.
 */
?>

<div class="ppy theme-blue" id="<?php print $popeye_id ?>">
  <ul class="ppy-imglist">
    <?php foreach ($rows as $row): ?>
      <?php print $row ?>
    <?php endforeach; ?>
  </ul>
  <div class="ppy-outer">
    <div class="ppy-stage-wrap">
      <div class="ppy-stage">
        <div class="ppy-counter">
          <strong class="ppy-current"></strong> / <strong class="ppy-total"></strong>
        </div>
      </div>
    </div>
    <div class="ppy-nav">
      <div class="ppy-nav-wrap">
        <a class="ppy-next" title="<?php print t('Next image'); ?>"><?php print t('Next image'); ?></a>
        <a class="ppy-prev" title="<?php print t('Previous image'); ?>"><?php print t('Previous image'); ?></a>
      </div>
    </div>
  </div>
</div>

<?php

/**
 * @file
 * Theme functions for popeye.
 */

/**
 * Returns HTML for an Colorbox image field formatter.
 */
function theme_popeye_field_formatter($variables) {
  $items = $variables['item'];
  $thumb_style = $variables['display_settings']['popeye_thumb_style'];
  $image_style = $variables['display_settings']['popeye_image_style'];
  $caption_setting = $variables['display_settings']['popeye_caption'];
  if (count($items) >= 1) {
    foreach ($items as $item) {
      // Hanlde Caption display.
      switch ($caption_setting) {
        case 'auto':
          // If the title is empty use alt or the node title in that order.
          if (!empty($item['title'])) {
            $caption = $item['title'];
          }
          elseif (!empty($item['alt'])) {
            $caption = $item['alt'];
          }
          else {
            $caption = '';
          }
          break;
        case 'title':
          $caption = $item['title'];
          break;
        case 'alt':
          $caption = $item['alt'];
          break;
        default:
          $caption = '';
      }
      // Define params for popeye image.
      $params_image = array(
        'path' => $item['uri'],
        'style_name' => $thumb_style,
        'alt' => $caption,
      );

      $thumbnail_image = theme('image_style', $params_image);
      $link_big_image = image_style_url($image_style, $item['uri']);
      $rows[] = '<li>' . l($thumbnail_image, $link_big_image, array('html' => TRUE)) . '</li>';
    }

    $popeye_id = 'popeye' . '_' . $variables['field']['field_name'] . '_' . $variables['node']->nid;
    $style = $variables['display_settings']['popeye_style'];
    $params = $variables['display_settings']['adv'];
    $cfg[$popeye_id] = $params;
    drupal_add_js(array('popeye' => $cfg), 'setting');
    _popeye_add_libraries($style);
    return theme('popeye_style_' . $style, array('rows' => $rows, 'popeye_id' => $popeye_id));
  }
}

/**
 * Implementation of template_process for views-view-popeye.
 */
function template_preprocess_views_view_popeye(&$vars) {
  if (!empty($vars['view']->live_preview)) {
    drupal_set_message(t('The jquery.popeye plugin cannot be viewed in the preview.'), 'warning');
    return;
  }
  $view = $vars['view'];
  $options = $vars['options'];
  // Check if data is available.
  if (count($view->result) > 0) {
    // Preprocess CSS information for the template.
    $vars['popeye_id'] = 'popeye-' . $view->name . '-' . $view->current_display;
    $vars['popeye_theme'] = $options['theme'];
    $cfg = array();

    $params = array(
      'navigation' => $options['navigation'],
      'caption' => $options['caption'],
      'zindex' => $options['zindex'],
      'direction' => $options['direction'],
      'duration' => $options['duration'],
      'opacity' => $options['opacity'],
      'easing' => $options['easing'],
      'slidespeed' => $options['slidespeed'],
      'autoslide' => $options['autoslide'],
    );

    $cfg[$vars['popeye_id']] = $params;
    drupal_add_js(array('popeye' => $cfg), 'setting');
    _popeye_add_libraries($options['theme']);
  }
  else {
    drupal_set_message(t('There is no data available to show on the popeye'), 'warning');
  }
}

/**
 * Implementation of template preprocess for the view fields.
 */
function template_preprocess_popeye_view_popeyefields(&$vars) {
  $view = &$vars['view'];
  $options = $vars['options'];
  $row = $vars['row'];

  foreach (array_keys($view->field) as $id) {
    $field_output = $view->field[$id]->theme($vars['row']);
    $link_big_image = '';
    switch ($id) {
      case $options['big_image']:
        $field_image_name = "field_{$options['big_image']}";
        $image_url = $row->{$field_image_name}[0]['raw']['uri'];
        $presetname = $row->{$field_image_name}[0]['rendered']['#image_style'];
        $link_big_image = image_style_url($presetname, $image_url);
        break;

      case $options['thumb_image']:
        $field_image_name = "field_{$options['thumb_image']}";
        $thumbnail_image_uri = $row->{$field_image_name}[0]['raw']['uri'];
        $thumb_presetname = $row->{$field_image_name}[0]['rendered']['#image_style'];
        break;

      case $options['caption']:
        $caption = $field_output;
        break;
    }
  }
  // Define params for popeye thumbnail.
  $params_image = array(
    'path' => $thumbnail_image_uri,
    'style_name' => $thumb_presetname,
    'alt' => $caption,
  );
  $thumbnail_image = theme('image_style', $params_image);
  $vars['item'] = l($thumbnail_image, $link_big_image, array('html' => TRUE));
}

<?php
/**
 * @file
 * This template includes the popeye view.
 */
?>

<?php if ($rows): ?>
  <div class="ppy theme-<?php print $popeye_theme ?>" id="<?php print $popeye_id ?>">
    <ul class="ppy-imglist">
      <?php foreach ($rows as $row): ?>
        <?php print $row ?>
      <?php endforeach; ?>
    </ul>
    <?php if ($popeye_theme == 'blue'): ?>
      <div class="ppy-outer">
        <div class="ppy-stage-wrap">
          <div class="ppy-stage">
            <div class="ppy-counter">
              <strong class="ppy-current"></strong> / <strong class="ppy-total"></strong>
            </div>
          </div>
        </div>
        <div class="ppy-nav">
          <div class="ppy-nav-wrap">
            <a class="ppy-next" title="<?php print t('Next image'); ?>"><?php print t('Next image'); ?></a>
            <a class="ppy-prev" title="<?php print t('Previous image'); ?>"><?php print t('Previous image'); ?></a>
          </div>
        </div>
      </div>
      <?php elseif ($popeye_theme == 'dark'):
      ?>
      <div class="ppy-outer">
        <div class="ppy-stage">
          <div class="ppy-nav">
            <div class="nav-wrap">
              <a class="ppy-prev" title="<?php print t('Previous image'); ?>"><?php print t('Previous image'); ?></a>
              <a class="ppy-play" title="<?php print t('Play Slideshow'); ?>"><?php print t('Play Slideshow'); ?></a>
              <a class="ppy-pause" title="<?php print t('Stop Slideshow'); ?>"><?php print t('Stop Slideshow'); ?></a>
              <a class="ppy-next" title="<?php print t('Next image'); ?>"><?php print t('Next image'); ?></a>
            </div>
          </div>
          <div class="ppy-counter">
            <strong class="ppy-current"></strong> / <strong class="ppy-total"></strong>
          </div>
        </div>
        <div class="ppy-caption">
          <span class="ppy-text"></span>
        </div>
      </div>
    <?php else: ?>
      <div class="ppy-outer">
        <div class="ppy-stage">
          <div class="ppy-nav">
            <a class="ppy-prev" title="<?php print t('Previous image'); ?>"><?php print t('Previous image'); ?></a>
            <a class="ppy-switch-enlarge" title="<?php print t('Enlarge'); ?>"><?php print t('Enlarge'); ?></a>
            <a class="ppy-switch-compact" title="<?php print t('Close'); ?>"><?php print t('Close'); ?></a>
            <a class="ppy-next" title="<?php print t('Next image'); ?>"><?php print t('Next image'); ?></a>
          </div>
        </div>
      </div>
      <div class="ppy-caption">
        <div class="ppy-counter">
          <?php print t('Image !ppy-current of !ppy-total', array('!ppy-current' => '<span class="ppy-current"></span>', '!ppy-total' => '<strong class="ppy-total"></strong>'), array('context' => 'popeye')); ?>
        </div>
        <span class="ppy-text"></span>
      </div>
    <?php endif; ?>
  </div>
  <?php elseif ($empty):
  ?>
  <div class="view-empty">
    <?php print $empty; ?>
  </div>
<?php endif; ?>

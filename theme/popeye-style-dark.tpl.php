<?php
/**
 * @file
 * This dark-style-template includes the popeye field_formatter.
 */
?>

<div class="ppy theme-dark" id="<?php print $popeye_id ?>">
  <ul class="ppy-imglist">
    <?php foreach ($rows as $row): ?>
      <?php print $row ?>
    <?php endforeach; ?>
  </ul>
  <div class="ppy-outer">
    <div class="ppy-stage">
      <div class="ppy-nav">
        <div class="nav-wrap">
          <a class="ppy-prev" title="<?php print t('Previous image'); ?>"><?php print t('Previous image'); ?></a>
          <a class="ppy-play" title="<?php print t('Play Slideshow'); ?>"><?php print t('Play Slideshow'); ?></a>
          <a class="ppy-pause" title="<?php print t('Stop Slideshow'); ?>"><?php print t('Stop Slideshow'); ?></a>
          <a class="ppy-next" title="<?php print t('Next image'); ?>"><?php print t('Next image'); ?></a>
        </div>
      </div>
      <div class="ppy-counter">
        <strong class="ppy-current"></strong> / <strong class="ppy-total"></strong>
      </div>
    </div>
    <div class="ppy-caption">
      <span class="ppy-text"></span>
    </div>
  </div>
</div>

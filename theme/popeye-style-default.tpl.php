<?php
/**
 * @file
 * This blue-style-template includes the popeye field_formatter.
 */
?>
<div class="ppy theme-default" id="<?php print $popeye_id; ?>">
  <ul class="ppy-imglist">
    <?php foreach ($rows as $row): ?>
      <?php print $row ?>
    <?php endforeach; ?>
  </ul>
  <div class="ppy-outer">
    <div class="ppy-stage">
      <div class="ppy-nav">
        <a class="ppy-prev" title="<?php print t('Previous image'); ?>"><?php print t('Previous image'); ?></a>
        <a class="ppy-switch-enlarge" title="<?php print t('Enlarge'); ?>"><?php print t('Enlarge'); ?></a>
        <a class="ppy-switch-compact" title="<?php print t('Close'); ?>"><?php print t('Close'); ?></a>
        <a class="ppy-next" title="<?php print t('Next image'); ?>"><?php print t('Next image'); ?></a>
      </div>
    </div>
  </div>
  <div class="ppy-caption">
    <div class="ppy-counter">
      <?php print t('Image !ppy-current of !ppy-total', array('!ppy-current' => '<span class="ppy-current"></span>', '!ppy-total' => '<strong class="ppy-total"></strong>'), array('context' => 'popeye')); ?>
    </div>
    <span class="ppy-text"></span>
  </div>
</div>

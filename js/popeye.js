(function ($) {
  Drupal.behaviors.popeye = {
    attach:  function(context) {
      if(Drupal.settings.popeye) {
        $.each(Drupal.settings.popeye, function(id, data) {
          Drupal.popeye.createWidget(id,data);
        });
      }
    }
  };
  Drupal.popeye = {
    createWidget: function(id,args) {
      args = eval(args);
      $('#' + id).popeye(args);
    }
  };
})(jQuery);

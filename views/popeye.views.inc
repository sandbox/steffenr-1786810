<?php

/**
 * @file
 * Defines the View Style Plugins for popeye.
 */

/**
 * Implements hook_views_plugins().
 */
function popeye_views_plugins() {
  $path = drupal_get_path('module', 'popeye');
  return array(
    'module' => 'popeye',
    'style' => array(
      'popeye' => array(
        'title' => t('Views jQuery.popeye'),
        'help' => t('Displays content on jQuery popeye.'),
        'handler' => 'popeye_views_plugin_style',
        'uses options' => TRUE,
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
        'path' => "$path/theme",
        'theme' => "views_view_popeye",
        'theme file' =>  'popeye.theme.inc',
        'theme path' => "$path/theme",
      ),
    ),
    'row' => array(
      'popeye' => array(
        'title' => t('Views jQuery.popeye'),
        'help' => t('Choose the fields to display in Views popeye.'),
        'handler' => 'popeye_plugin_row_popeyefields',
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
        'path' => "$path/theme",
        'theme' => "popeye_view_popeyefields",
        'theme file' => 'popeye.theme.inc',
        'theme path' => "$path/theme",
      ),
    ),
  );
}

<?php

/**
 * @file
 * Defines the View Style Plugins for popeye module.
 */

/**
 * Extending the view_plugin_style class to provide a timeline view style.
 */
class popeye_views_plugin_style extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    unset($options['grouping']);
    $options['theme'] = array('default' => 'default');
    $options['navigation'] = array('default' => 'hover');
    $options['caption'] = array('default' => 'hover');
    $options['zindex'] = array('default' => 10000);
    $options['direction'] = array('default' => 'right');
    $options['duration'] = array('default' => 240);
    $options['opacity'] = array('default' => '0.8');
    $options['easing'] = array('default' => 'swing');
    $options['slidespeed'] = array('default' => 2000);
    $options['autoslide'] = array('default' => 'false');
    return $options;
  }

  /**
   * Show a form to edit the style options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['grouping']);

    // Appearance settings.
    $popeye_theme = array(
      'default' => t('default Style'),
      'blue' => t('Style 1 (blue)'),
      'dark' => t('Style 2 (dark)'),
    );

    $form['theme'] = array(
      '#type' => 'select',
      '#title' => t('Select a theme'),
      '#default_value' => $this->options['theme'],
      '#options' => $popeye_theme,
      '#required' => TRUE,
    );

    $popeye_navigation = array(
      'hover' => t('show on mouseover'),
      'permanent' => t('show permanently'),
    );
    $form['navigation'] = array(
      '#type' => 'select',
      '#title' => t('Navigation'),
      '#default_value' => $this->options['navigation'],
      '#options' => $popeye_navigation,
      '#description' => t("The visibility of the navigation. Can be 'hover' (show on mouseover) or 'permanent'"),
    );

    $popeye_caption = array(
      'hover' => t('show on mouseover'),
      'permanent' => t('show permanently'),
      'false' => t("don't show caption"),
    );
    $form['caption'] = array(
      '#type' => 'select',
      '#title' => t('Caption'),
      '#default_value' => $this->options['navigation'],
      '#options' => $popeye_caption,
      '#description' => t("The visibility of the navigation. Can be 'hover' (show on mouseover) or 'permanent' or false (don't show caption)"),
    );

    $form['zindex'] = array(
      '#type' => 'textfield',
      '#title' => t('z-index'),
      '#default_value' => $this->options['zindex'],
      '#description' => t("z-index of the expanded popeye-box. Enter a z-index that works well with your site and doesn't overlay your site's navigational elements like dropdowns."),
    );

    $popeye_direction = array(
      'right' => t('right'),
      'left' => t('left'),
    );

    $form['direction'] = array(
      '#type' => 'select',
      '#title' => t('Direction'),
      '#default_value' => $this->options['direction'],
      '#options' => $popeye_direction,
      '#description' => t("direction that popeye-box opens, can be 'left' or 'right'"),
    );

    $popeye_duration = array(
      '100' => '100',
      '150' => '150',
      '200' => '200',
      '250' => '250',
      '300' => '300',
      '350' => '350',
      '400' => '400',
      '450' => '450',
      '500' => '500',
      '550' => '550',
      '600' => '600',
    );
    $form['duration'] = array(
      '#type' => 'select',
      '#title' => t('duration'),
      '#default_value' => $this->options['duration'],
      '#options' => $popeye_duration,
      '#description' => t('duration of transitional effect when enlarging or closing the box'),
    );

    $popeye_opacity = array(
      '0.1' => '0.1',
      '0.2' => '0.2',
      '0.3' => '0.3',
      '0.4' => '0.4',
      '0.5' => '0.5',
      '0.6' => '0.6',
      '0.6' => '0.6',
      '0.7' => '0.7',
      '0.8' => '0.8',
      '0.9' => '0.8',
      '1' => '1',
    );
    $form['opacity'] = array(
      '#type' => 'select',
      '#title' => t('opacity'),
      '#default_value' => $this->options['opacity'],
      '#options' => $popeye_opacity,
      '#description' => t('opacity of navigational overlay.'),
    );

    $popeye_easing = array(
      'swing' => 'swing',
      'linear' => 'linear',
    );
    $form['easing'] = array(
      '#type' => 'select',
      '#title' => t('easing'),
      '#default_value' => $this->options['easing'],
      '#options' => $popeye_easing,
      '#description' => t("easing type, can be 'swing', 'linear'."),
    );

    $popeye_slidespeed = array(
      '500' => '500',
      '1000' => '1000',
      '1500' => '1500',
      '2000' => '2000',
      '2500' => '2500',
    );
    $form['slidespeed'] = array(
      '#type' => 'select',
      '#title' => t('slidespeed'),
      '#default_value' => $this->options['slidespeed'],
      '#options' => $popeye_slidespeed,
      '#description' => t('slideshow speed in milliseconds.'),
    );

    $popeye_autoslide = array(
      'true' => 'true',
      'false' => 'false',
    );
    $form['autoslide'] = array(
      '#type' => 'select',
      '#title' => t('auto_play'),
      '#default_value' => $this->options['autoslide'],
      '#options' => $popeye_autoslide,
      '#description' => t('Should the slideshow start automatically?'),
    );
  }
}

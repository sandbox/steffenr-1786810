<?php

/**
 * @file
 * Contains the base row style plugin.
 */

/**
 * The basic 'fields' row plugin
 *
 * This displays fields one after another, giving options for inline
 * or not.
 *
 * @ingroup views_row_plugins
 */
class popeye_plugin_row_popeyefields extends views_plugin_row {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['big_image'] = array('default' => '');
    $options['thumb_image'] = array('default' => '');
    $options['caption'] = array('default' => '');
    return $options;
  }

  /**
   * Show a form to edit the style options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['grouping']);

    // Pre-build all of our option lists for the dials and switches that follow.
    $fields = array('' => t('<none>'));
    $fields_images = array('' => t('<none>'));
    foreach ($this->display->handler->get_handlers('field') as $field => $handler) {
      // Filter images.
      if ($handler->options['type'] == 'image') {
        $presetname = ' (' . $handler->options['settings']['image_style'] . ')';
        if ($label = $handler->label()) {
          $fields_images[$field] = $label . $presetname;
        }
        else {
          $fields_images[$field] = $handler->ui_name() . $presetname;
        }
      }
      else {
        if ($label = $handler->label()) {
          $fields[$field] = $label;
        }
        else {
          $fields[$field] = $handler->ui_name();
        }
      }
    }
    $form['big_image'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Big image'),
      '#options' => $fields_images,
      '#default_value' => $this->options['big_image'],
      '#description' => t('Select the field that will be used as the big image'),
    );

    $form['thumb_image'] = array(
      '#type' => 'select',
      '#title' => t('Thumb image'),
      '#options' => $fields_images,
      '#default_value' => $this->options['thumb_image'],
      '#description' => t('Select the field that will be used as the thumb image'),
    );

    $form['caption'] = array(
      '#type' => 'select',
      '#title' => t('Caption field'),
      '#options' => $fields,
      '#default_value' => $this->options['caption'],
      '#description' => t('Select the field that will be used as the caption.'),
    );
  }
}
